
var map;
var markers = [];
var infoWindow;

function initMap() {
   var Ethiopia = {lat: 34.063380, lng: -118.358080};
    map = new google.maps.Map(document.getElementById('map'), {
      center: Ethiopia,
      zoom: 11,
      mapTypeId: 'roadmap',
      mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
    });
    infoWindow = new google.maps.InfoWindow();
    searchStores();
    //displayStore();
    //showStoresMarkers();
    //setOnClicklistener();
    //searchStores();
    //setOnClicklistener();
  }

 
  /*function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 2,
    center: {lat: 4.063380, lng:-118.358080 }
  });
  for (var [index,store] in stores){
    var latlng={lat:store["coordinates"]["latitude"],
                 lng:store["coordinates"]["longitude"]}
  marker = new google.maps.Marker({
    map: map,
    draggable: true,
    animation: google.maps.Animation.DROP,
    position: latlng
    //{lat: 9.1450005, lng: 40.4896736}
  });
}
  marker.addListener('click', toggleBounce);
}

function toggleBounce() {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}*/
function displayStore(stores){
  var storeHTML ='';

  for(var [index,store] of stores.entries()){
    var address =store['addressLines'];
    var phone = store['phoneNumber'];
  
    storeHTML += `
    <div class="store-container">
    <div class="store-info-container">
    <div class="store-address">
        <span >${address[0]} </span>
        <span>${address[1]}</span>
        
    </div>
    <div class="store-phone-number">
        <span>${phone}</span>
    </div>
</div>
<div class="store-number-container">
    <div class="store-number">${++index}</div>
</div>
</div>    
`
document.querySelector('.store-list').innerHTML = storeHTML;
}
//document.querySelector('.store-list').innerHTML = storeHTML;
  //stores.map(function(store, index){

  //})
}

function showStoresMarkers(stores){
  console.log("not working");
  var bounds = new google.maps.LatLngBounds();
  for(var [index,store] of stores.entries()){ 

     var latlng = new google.maps.LatLng(
      store["coordinates"]["latitude"],
      store["coordinates"]["longitude"]);
      
      var name = store["name"];
      var address = store["addressLines"][0];
      var status = store["openStatusText"];
      var phone = store['phoneNumber'];
     
    bounds.extend(latlng);
    createMarker(latlng,name,address, status,phone, index+1);
    console.log("not working");
  }
  map.fitBounds(bounds);
 
  
}
       
    
    function createMarker(latlng,name, address,status,phone,  index) {
      var image = 'C:/Users/samra/Downloads/images.png';

     // var html = "<b>" + name  +  "</b> <br/>" + address;
       var html = `
       <div class="test" style="width:200px;height:100px">
           <div  style=" font-weight: bold;color:lightgreen;">${name}</div>
           <div style=" border-bottom: 1px solid grey; margin-bottom:5px;">${status}</div>
           
           <div  id="test" style="font-size: 15px;" >
             <div id="circle" >
              <i  class="fas fa-location-arrow" ></i>
              </div>${address} 
             
           </div>  
           <div  style="font-size:15px; display:flex;">
              <div class="phone">
              <i class="fas fa-phone"></i>
              </div>${phone}
           </div>         
       </div>`;
      var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        label: index.toString(),
        icon: image
      });
     google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
      
      markers.push(marker);
    }

function codeAddress(address) {
  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == 'OK') {
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
      });
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}
function setOnClicklistener(){
  var storeElements = document.querySelectorAll('.store-container');
  console.log(storeElements);
   storeElements.forEach( function(storeElement, index){

       storeElement.addEventListener('click', function(){
         new google.maps.event.trigger(markers[index],'click');
       });
  });
}
 
function clearLocations(){
  infoWindow.close();
  for (var i = 0; i<markers.length; i++){
    markers[i].setMap(null);
  }
  markers.length = 0;
}
function searchStores(){
  var foundStores = [];
  var zipCode = document.getElementById('zip-code-input').value;
  if ( zipCode ){
  for ( var  store of stores){
    var postalCode = store["address"]["postalCode"].substring(0,5);
    if (postalCode == zipCode){
      foundStores.push(store);
    }
  }

}
    else{
      foundStores = stores;
    }
      clearLocations();
      displayStore(foundStores);
      showStoresMarkers(foundStores);
      setOnClicklistener();
    }
